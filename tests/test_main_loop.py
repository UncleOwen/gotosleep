# These are tests. Do we really need docstrings?
# pylint: disable=missing-docstring


from unittest.mock import (
    call,
    MagicMock,
    patch,
)

from gotosleep.main import (
    main,
)

from gotosleep.registry import (
    ureg,
    Q_,
)


def test_volumecalculator_is_fed_with_values_from_time_dot_monotonic() -> None:
    with patch(
        'gotosleep.main.monotonic',
        MagicMock(side_effect=[
            Q_(10, 'seconds'),
            Q_(10, 'seconds'),
            Q_(11, 'seconds'),
            Q_(12, 'seconds'),
        ])
    ):
        with patch(
            'gotosleep.main.VolumeCalculator', MagicMock()
        ) as vol_calc_mock:
            with patch(
                'gotosleep.main.set_volume',
                MagicMock()
            ):
                with patch(
                    'gotosleep.main.sleep',
                    MagicMock(side_effect=[None, None, KeyboardInterrupt()])
                ):
                    main()
    expected = [
        call(Q_(10, 'seconds')),
        call().calc(Q_(10, 'seconds')),
        call().calc(Q_(11, 'seconds')),
        call().calc(Q_(12, 'seconds')),
    ]
    assert expected == vol_calc_mock.mock_calls


def test_returns_from_volumecalculator_are_passed_to_set_volume() -> None:
    with patch(
        'gotosleep.main.monotonic',
        MagicMock(side_effect=[10, 10, 11, 12])
    ):
        with patch(
            'gotosleep.main.VolumeCalculator',
            MagicMock()
        ) as vol_calc_mock:
            vol_calc_mock.return_value.calc = MagicMock(
                side_effect=[
                    Q_(.75, ureg.dimensionless),
                    Q_(.5, ureg.dimensionless),
                    Q_(.25, ureg.dimensionless),
                ]
            )
            with patch(
                'gotosleep.main.set_volume',
                MagicMock()
            ) as set_volume_mock:
                with patch(
                    'gotosleep.main.sleep',
                    MagicMock(side_effect=[None, None, KeyboardInterrupt()])
                ):
                    main()
    expected = [
        call(Q_(.75, ureg.dimensionless)),
        call(Q_(.5, ureg.dimensionless)),
        call(Q_(.25, ureg.dimensionless)),
        call(Q_(1, ureg.dimensionless)),
    ]
    assert expected == set_volume_mock.mock_calls
