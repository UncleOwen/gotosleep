# These are tests. Do we really need docstrings?
# pylint: disable=missing-docstring


import pytest

from gotosleep import (
    VolumeCalculator,
    Q_,
)


@pytest.fixture(name='vol_calc')
def fixture_vol_calc() -> VolumeCalculator:
    return VolumeCalculator(Q_('1 day'))


def test_initial_value_is_one(vol_calc: VolumeCalculator) -> None:
    assert (vol_calc.calc(vol_calc.starttime)
            == Q_(1, 'dimensionless'))


def test_after_three_hours_value_is_zero(vol_calc: VolumeCalculator) -> None:
    assert (vol_calc.calc(vol_calc.starttime + Q_('3 hours'))
            == Q_(0, 'dimensionless'))


def test_at_half_time_the_volume_is_half(vol_calc: VolumeCalculator) -> None:
    assert (vol_calc.calc(vol_calc.starttime + Q_('90 minutes'))
            == Q_(0.5, 'dimensionless'))


def test_after_time_expires_volume_is_still_zero(
    vol_calc: VolumeCalculator
) -> None:
    assert (vol_calc.calc(vol_calc.starttime + Q_('1 day'))
            == Q_(0, 'dimensionless'))
