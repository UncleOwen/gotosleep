"""
gradually reduce output volume so I can go to sleep
"""

from pint import Quantity

from .registry import Q_


class VolumeCalculator():
    """
    Calculate output volume based on time progressed since the program started.
    """

    def __init__(self, starttime: Quantity) -> None:
        """
        starttime is the value of some clock
        """
        self._starttime = starttime

    @property
    def starttime(self) -> Quantity:
        """
        return the starttime given in __init__
        """
        return self._starttime

    def calc(self, now: Quantity) -> Quantity:
        """
        calculate the volume.
        now is a later value of the same clock used in __init__
        """

        result = 1 - (now - self._starttime)/(Q_('3 hours'))
        return result if 0 <= result else Q_('0 dimensionless')
