'''
helper functions to access pulse audio.
'''

from pulsectl import Pulse  # type: ignore[import]

from .registry import ureg


@ureg.wraps(None, ureg.dimensionless)
def set_volume(volume: float) -> None:
    '''
    sets global output volume.
    volume is in the range
    0 (mute)
    1 (normal full volume)
    2 (output boosted)
    '''
    with Pulse('gotosleep') as pulse:
        print(f'Setting volume to {100*volume:3.1f}%')
        sink = pulse.sink_list()[0]
        pulse.volume_set_all_chans(sink, volume)
