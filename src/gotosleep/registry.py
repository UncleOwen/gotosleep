''' this file defines the canonical unitregistry for this application '''

from pint import UnitRegistry
ureg = UnitRegistry()
Q_ = ureg.Quantity
