'''
This file contains the main loop.
'''

from time import (
    monotonic as _monotonic,
    sleep as _sleep,
)

from .registry import Q_, ureg
from .volumecalculator import VolumeCalculator
from .pulse import set_volume

# pylint: disable=unnecessary-lambda
# These lambdas are necessary, because pint cannot handle built-in functions.
# https://stackoverflow.com/questions/70755589/pint-is-wrapping-built-in-functions-with-ureg-wraps-impossible
sleep = ureg.wraps(None, ureg.seconds)(lambda t: _sleep(t))
monotonic = ureg.wraps(ureg.seconds, [])(lambda: _monotonic())
# pylint: enable=unnecessary-lambda


def main() -> None:
    '''
    main loop
    '''
    try:
        vol_calc = VolumeCalculator(monotonic())
        while True:
            set_volume(vol_calc.calc(monotonic()))
            sleep(Q_(1, 'second'))
    except KeyboardInterrupt:
        set_volume(Q_(1, ureg.dimensionless))


if __name__ == '__main__':
    main()
